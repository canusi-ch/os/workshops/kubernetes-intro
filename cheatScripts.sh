## Get status all pods
kubectl describe pods



## Scale up/down
kubectl scale --replicas=0 "deployment/job-elasticsearch"
kubectl delete "deployment/job-elasticsearch"
kubectl delete service "serv-elasticsearch"


## Shell into pod
kubectl exec -it job-elasticsearch-79dcc74874-txjf9  /bin/bash

## Get logs for pod
kubectl logs job-elasticsearch-79dcc74874-txjf9 	

