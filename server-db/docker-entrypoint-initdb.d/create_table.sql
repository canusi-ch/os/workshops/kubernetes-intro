


create table test_table
(
	user_job_id integer primary key,
	session_id integer,
	source_id integer,
	user_hash_id varchar(255),
	flag_process_status varchar(255),
	etl_date_of_insert time,
	etl_date_of_update time
);

