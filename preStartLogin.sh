
DIR="$PWD"
GCPFILE=gcp.json
GCP_PROJECT="tutorial-269012"

gcloud auth activate-service-account --key-file="$DIR"/../"$GCPFILE"
gcloud config set project "$GCP_PROJECT"
gcloud container clusters get-credentials  --region us-central1-a     standard-cluster-1

gcloud auth configure-docker
