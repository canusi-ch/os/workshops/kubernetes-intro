set -e 

export image_version="$1"
export project_name="$2"

cat job-elasticsearch.yaml | envsubst > ./built_job-elasticsearch.yaml
kubectl -f ./built_job-elasticsearch.yaml apply

cat job-kibana.yaml | envsubst > ./built_job-kibana.yaml
kubectl -f ./built_job-kibana.yaml apply

cat volume-server-db.yaml | envsubst > ./built_volume-server-db.yaml
kubectl -f ./built_volume-server-db.yaml apply

cat job-server-db.yaml | envsubst > ./built_job-server-db.yaml
kubectl -f ./built_job-server-db.yaml apply



cat serv-server-db.yaml | envsubst > ./built_serv-server-db.yaml
kubectl -f ./built_serv-server-db.yaml apply

cat serv-elasticsearch.yaml | envsubst > ./built_serv-elasticsearch.yaml
kubectl -f ./built_serv-elasticsearch.yaml apply

cat serv-kibana.yaml | envsubst > ./built_serv-kibana.yaml
kubectl -f ./built_serv-kibana.yaml apply


set -e

