
VERSION="0.1"
set -x
pwdInit="$PWD"
projectName="tutorial-269012"


cd "$pwdInit"/server-db
docker build -t gcr.io/$projectName/dock-server-database:$VERSION .


docker push gcr.io/$projectName/dock-server-database:$VERSION


cd "$pwdInit/k8s/"
./deploy.sh $VERSION $projectName


kubectl rollout restart deployments/job-server-db
kubectl rollout restart deployments/job-elasticsearch
kubectl rollout restart deployments/job-kibana

